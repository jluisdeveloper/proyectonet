﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrayDummy
{
    public class CalculadoraDummy
    {
        
        public int suma(int a, int b)
        {
            int suma = a + b;
            return suma;
        }

        public int resta(int a, int b)
        {
            int resta = a - b;
            return resta;
        }

        public int multiplicacion(int a, int b)
        {
            int multi = a * b;
            return multi;
        }

        public int division(int a, int b)
        {
            int divi = a / b;
            return divi;
        }
    }
}
