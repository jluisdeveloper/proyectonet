﻿using ClassLibrayDummy;
using NUnit.Framework;

namespace UnitTestClassLibraryDummy
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void sumaTest()
        {
            CalculadoraDummy calc = new CalculadoraDummy();
            int result = calc.suma(20, 10);
            Assert.AreEqual(30, result);
        }

        [Test]
        public void restaTest()
        {
            CalculadoraDummy calc = new CalculadoraDummy();
            int result = calc.resta(20, 10);
            Assert.AreEqual(10, result);
        }

        [Test]
        public void multiplicacionTest()
        {
            CalculadoraDummy calc = new CalculadoraDummy();
            int result = calc.multiplicacion(20, 10);
            Assert.AreEqual(200, result);
        }

        [Test]
        public void divisionTest()
        {
            CalculadoraDummy calc = new CalculadoraDummy();
            int result = calc.division(20, 10);
            Assert.AreEqual(2, result);
        }
    }
}
